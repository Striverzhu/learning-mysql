create database test_db;

use test_db;
create table tb_emp2(
	id int(11) primary key,
    name varchar(25),
    deptId int(11),
    salary float
);

use test_db;
create table tb_emp3(
	id int(11),
    name varchar(25),
    deptId int(11),
    salary float,
    primary key(id)
);

use test_db;
create table tb_emp4(
	name varchar(25),
    deptId int(11),
    salary float,
    primary key(name,deptId)
);

#多字段主键，不支持以下方式定义
use test_db;
create table yy(
	id int(12) primary key,
    name varchar(23) primary key,
    age int(3)
);

use test_db;
create table tb_dept1(
	id int(11) primary key,
    name varchar(22) not null,
    location varchar(50)
);

use test_db;
create table tb_emp5(
	id int(11) primary key,
    name varchar(25),
    deptId int(11),
    salary float,
    constraint fk_emp_dept1 foreign key(deptId) references tb_dept1(id)
);

use test_db;
create table tb_emp6(
	id int(11) primary key,
    name varchar(25),
    deptId int(11),
    salary float,
    foreign key(deptId) references tb_dept1(id)
);

use test_db;
create table tb_dept2(
	id int(11) primary key,
    name varchar(22),
    location varchar(53),
    unique(name)
);

use test_db;
create table tb_dept3(
	id int(11) primary key auto_increment,
    name varchar(45),
    location varchar(54)
);

use test_db;
create table tb_emp7(
	id int(11) primary key,
    name varchar(25) not null,
    deptId int(11) default 1111,
    salary float
);

use test_db;
create table tb_emp8(
	id int(11) primary key auto_increment,
    name varchar(25) not null,
    deptId int(11),
    salary float
);

use test_db;
insert into tb_emp8 (name,salary) values('lucy',2000),('lora',5000),('karlin',7540);

desc tb_emp8;
describe tb_emp7;

show create table tb_emp8;

show tables;

alter table tb_dept3 rename tb_deptment3;

desc tb_dept1;

alter table tb_dept1 modify name varchar(30);

alter table tb_dept1 change loc location varchar(60);

alter table tb_dept1 add managerId int(10);

alter table tb_deptment3 engine=myisam;

describe tb_deptment3;

show create table tb_deptment3;

create table tb_emp9(
	id int(11) primary key,
    name varchar(25),
    deptId int(11),
    salary float,
    constraint fk_emp_dept foreign key(deptId) references tb_dept1(id)
);

alter table tb_emp9 drop foreign key fk_emp_dept;

describe tb_emp9;

describe tb_dept1;