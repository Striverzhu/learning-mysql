create database company;

use company;
create table offices(
	officeCode int(10) primary key,
    city varchar(50) not null,
    address varchar(50),
    country varchar(50) not null,
    postalCode varchar(15) unique
);

use company;
create table employees(
	employeeNumber int(11) primary key auto_increment,
    lastName varchar(50) not null,
    firstName varchar(50) not null,
    mobile varchar(25) unique,
    officeCode int(10) not null,
    jobTitle varchar(50) not null,
    birth datetime not null,
    note varchar(25),
    sex varchar(5),
    constraint fk_company_offices foreign key (officeCode) references offices(officeCode)
);

use company;
alter table employees modify mobile varchar(25) after officeCode;

use company;
alter table employees change birth employ_birth datetime;